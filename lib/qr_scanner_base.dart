import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef void QRViewCreatedCallback(QRViewController controller);

class QRViewBase extends StatefulWidget {
  const QRViewBase({
    @required Key key,
    @required this.scanStreamController,
    this.onQRViewCreated,
    this.width = 0.0,
    this.height = 0.0,
    this.overlay,
  })  : assert(key != null),
        super(key: key);

  final StreamController<String> scanStreamController;
  final QRViewCreatedCallback onQRViewCreated;

  final ShapeBorder overlay;

  final double width;
  final double height;

  @override
  State<StatefulWidget> createState() => _QRViewBaseState();
}

class _QRViewBaseState extends State<QRViewBase> {

  @override
  Widget build(BuildContext context) {

    return _getPlatformQrView(widget.width, widget.height);
    // return Stack(
    //   children: [
        
    //     widget.overlay != null
    //         ? Container(
    //             decoration: ShapeDecoration(
    //               shape: widget.overlay,
    //             ),
    //           )
    //         : Container(),
    //   ],
    // );
  }

  Widget _getPlatformQrView(double width, double height) {

    Widget _platformQrView;
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        _platformQrView = AndroidView(
          viewType: 'net.touchcapture.qr.flutterqr/qrview',
          onPlatformViewCreated: _onPlatformViewCreated,
        );
        break;
      case TargetPlatform.iOS:
        _platformQrView = UiKitView(
          viewType: 'net.touchcapture.qr.flutterqr/qrview',
          onPlatformViewCreated: _onPlatformViewCreated,
          creationParams: _CreationParams.fromWidget(width, height).toMap(),
          creationParamsCodec: StandardMessageCodec(),
        );
        break;
      default:
        throw UnsupportedError(
            "Trying to use the default webview implementation for $defaultTargetPlatform but there isn't a default one");
    }
    return _platformQrView;
  }

  void _onPlatformViewCreated(int id) {
    QRViewController._(id, widget.key, widget.scanStreamController);

    // widget.onQRViewCreated();
  }
}

class _CreationParams {
  _CreationParams({this.width, this.height});

  static _CreationParams fromWidget(double width, double height) {
    return _CreationParams(
      width: width,
      height: height,
    );
  }

  final double width;
  final double height;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'width': width,
      'height': height,
    };
  }
}

class QRViewController {
  static const scanMethodCall = "onRecognizeQR";
  final MethodChannel _channel;

  QRViewController._(int id, GlobalKey qrKey, StreamController<String> streamController) : _channel = MethodChannel('net.touchcapture.qr.flutterqr/qrview_$id') {
    print(streamController);
    if (defaultTargetPlatform == TargetPlatform.iOS) {
       final RenderBox renderBox = qrKey.currentContext.findRenderObject();
       _channel.invokeMethod("setDimensions", {"width": renderBox.size.width, "height": renderBox.size.height});
    }
    _channel.setMethodCallHandler( (MethodCall call) async {
        switch (call.method) {
          case scanMethodCall:
            if (call.arguments != null) {
              streamController.sink.add(call.arguments.toString());
            }
        }
      },
    );
  }

  // void flipCamera() {
  //   _channel.invokeMethod("flipCamera");
  // }

  // void toggleFlash() {
  //   _channel.invokeMethod("toggleFlash");
  // }

  // void pauseCamera() {
  //   _channel.invokeMethod("pauseCamera");
  // }

  // void resumeCamera() {
  //   _channel.invokeMethod("resumeCamera");
  // }

  // void dispose() {
  //   _scanUpdateController.close();
  // }
}

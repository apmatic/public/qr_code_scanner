import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_view_wrapper.dart';

class QRView extends StatelessWidget {

  final StreamController<String> scanStreamController;

  QRView({@required this.scanStreamController});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return QRViewWrapper(
          scanStreamController: scanStreamController,
        );
      },
    );
  }
}
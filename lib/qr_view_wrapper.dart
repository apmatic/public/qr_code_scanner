import 'dart:async';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_scanner_base.dart';

class QRViewWrapper extends StatefulWidget {
  final StreamController<String> scanStreamController;

  QRViewWrapper({this.scanStreamController});

  @override
  _QRViewWrapperState createState() => _QRViewWrapperState();
}

class _QRViewWrapperState extends State<QRViewWrapper> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  double width = 0.0;
  double height = 0.0;
  bool hasDimensions = false;
  Timer _timer;

  
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => afterFirstLayout(context));
  }

  void afterFirstLayout(BuildContext context) {
    final RenderBox renderBox = context.findRenderObject();
    if (renderBox != null) {

      if ((width != renderBox.size.width) || (height != renderBox.size.height)) {
      
        setState(() {
          hasDimensions = false;
          width = renderBox.size.width;
          height = renderBox.size.height;
        });

        _timer?.cancel();
        _timer = new Timer(const Duration(milliseconds: 150), () {
          setState(() {
            hasDimensions = true;
          });
        });
      }
      
    }
  }

  @override
  void didUpdateWidget(QRViewWrapper oldWidget) {
    super.didUpdateWidget(oldWidget);
    WidgetsBinding.instance.addPostFrameCallback((_) => afterFirstLayout(context));
  }

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        Container(
          width: width,
          height: height,
          color: Colors.black,
        ),
        
        hasDimensions
          ? SizedBox(
            width: width,
            height: height,
            child: QRViewBase(
              key: qrKey,
              scanStreamController: widget.scanStreamController,
              width: width,
              height: height,
            ),
          )
          : Container(),
      ],
    );

    // if (hasDimensions) {

    //   return SizedBox(
    //     width: width,
    //     height: height,
    //     child: QRViewBase(
    //       key: qrKey,
    //       scanStreamController: widget.scanStreamController,
    //       width: width,
    //       height: height,
    //     ),
    //   );

    // } else {
    //   return Container(
    //     width: width,
    //     height: height,
    //     color: Colors.black,
    //     child: Text('Kamera startet', style: TextStyle(color: Colors.white),),
    //   );
    // }
  
  }
}
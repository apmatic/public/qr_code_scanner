import 'dart:async';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_view.dart';

void main() => runApp(MaterialApp(home: QRViewExample()));

class QRViewExample extends StatefulWidget {
  const QRViewExample({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  StreamController<String> _scanUpdateController = StreamController<String>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _scanUpdateController.stream.listen((scanData) {
      print('S: $scanData');
    });
  }

  @override
  void dispose() {
    _scanUpdateController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                height: 200.0,
                child: QRView(
                  scanStreamController: _scanUpdateController
                ),
              ),
            ),
            flex: 4,
          ),
          Expanded(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.red,
            ),
            flex: 2,
          )
        ],
      ),
    );
  }

}
